module Main
  ( main
  ) where

import qualified Data.Text.Lazy.IO.Utf8 as Utf8
import qualified Lorentz as L

import Lorentz.Contracts.Counter

main :: IO ()
main = do
  Utf8.writeFile "Counter.md" $
    L.buildMarkdownDoc $ L.finalizedAsIs counterContract
