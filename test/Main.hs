-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main
  ( main
  ) where

import Test.Tasty (defaultMain)

import Tree (tests)

main :: IO ()
main = tests >>= defaultMain
