-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.Contracts.Counter.Doc
  ( test_Documentation
  , test_Documentation_ordering
  ) where

import Test.Tasty (TestTree)

import Cleveland.Util
import Lorentz
import Lorentz.Test
import Michelson.Doc.Test (testNoGitInfo)

import Lorentz.Contracts.Counter

test_Documentation :: [TestTree]
test_Documentation =
  let tests = testLorentzDoc `excludeDocTests`
        [ testNoGitInfo  -- we do not include git info for simplicity
        ]
  in runDocTests tests counterContract

test_Documentation_ordering :: [TestTree]
test_Documentation_ordering =
  [ Proxy @DDescription `goesBefore` Proxy @DAuthorization
  ]
